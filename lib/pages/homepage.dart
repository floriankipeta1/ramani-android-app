import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:ramani_app/models/music.dart';
import 'package:ramani_app/src/routes.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ramani_app/src/sliderightroute.dart';

import 'description.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  List<Music> tracks = List();
  bool isLoading = false;

  getHttp(search) async {
    print('Start Fetching Data From api');
    setState(() {
      isLoading = true;
    });
    try {
      Response response = await Dio().get(URL + "${search}");

      if (response.statusCode == 200) {
        var responseBody = json.decode(response.data);

        var convertedData = responseBody["results"];
        print(convertedData);
        setState(() {
          tracks = (convertedData as List)
              .map((data) => new Music.fromJson(data))
              .toList();
          isLoading = false;
        });
      } else {
        throw Exception('Failed to Load Data');
      }
    } catch (e) {
      print(e);
    }
    print('Done Fetching Data from api');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        bottom: false,
        child: Container(
          decoration: BoxDecoration(color: Colors.pink[300]),
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  hintText: 'Search Music',
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide: BorderSide(color: Colors.black)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      borderSide: BorderSide(color: Colors.black)),
                ),
                onChanged: (text) {
                  getHttp(text);
                },
              ),
              Expanded(
                child: isLoading
                    ? SpinKitWave(
                        color: Colors.white,
                        size: 30.0,
                      )
                    : ListView.builder(
                        itemCount: tracks == null ? 0 : tracks.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                              padding: EdgeInsets.all(8.0),
                              height: 100.0,
                              child: FlatButton(
                                  padding: EdgeInsets.all(0.0),
                                  child: Row(
                                    children: <Widget>[
                                      tracks[index].artworkUrl100 != null ? Container(
                                        child: Image.network(
                                            tracks[index].artworkUrl100),
                                      ) : Container(),
                                      Wrap(
                                        crossAxisAlignment:
                                            WrapCrossAlignment.start,
                                        direction: Axis.vertical,
                                        children: <Widget>[
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                135,
                                            padding: const EdgeInsets.only(
                                                top: 8.0,
                                                left: 8.0,
                                                right: 8.0),
                                            child: tracks[index].trackName  != null ? Text(
                                              tracks[index].trackName,
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ) : Text('Empty Track Name',style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white)),
                                          ),
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width -
                                                135,
                                            padding: const EdgeInsets.only(
                                                top: 8.0,
                                                left: 8.0,
                                                right: 8.0),
                                            child: tracks[index].artistName != null ? Text(
                                              tracks[index].artistName,
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ) : Text('No Artist Name',style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white)),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                top: 8.0,
                                                left: 8.0,
                                                right: 8.0),
                                            child: tracks[index]
                                                .releaseDate != null ? Text(
                                              tracks[index]
                                                  .releaseDate
                                                  .substring(0, 4),
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ) : Text('Empty Release Date'),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  onPressed: () => Navigator.push(
                                        context,
                                        SlideRightRoute(
                                            page: Description(
                                          artistName: tracks[index].artistName,
                                          trackName: tracks[index].trackName,
                                          artworkUrl60:
                                              tracks[index].artworkUrl60,
                                          artworkUrl100:
                                              tracks[index].artworkUrl100,
                                          trackPrice: tracks[index].trackPrice,
                                          primaryGenreName:
                                              tracks[index].primaryGenreName,
                                          releaseDate:
                                              tracks[index].releaseDate,
                                          currency: tracks[index].currency,
                                        )),
                                      )));
                        }),
              )
            ],
          ),
        ),
      ),
    );
  }
}
