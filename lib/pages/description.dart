import 'package:flutter/material.dart';

class Description extends StatefulWidget {
  final String artistName;
  final String trackName;
  final String artworkUrl60;
  final String artworkUrl100;
  final double trackPrice;
  final String primaryGenreName;
  final String releaseDate;
  final String currency;

  const Description(
      {Key key,
      this.artistName,
      this.trackName,
      this.artworkUrl60,
      this.artworkUrl100,
      this.trackPrice,
      this.primaryGenreName,
      this.releaseDate,
      this.currency,})
      : super(key: key);

  @override
  _DescriptionState createState() => _DescriptionState();
}

class _DescriptionState extends State<Description> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.deepOrange,
        title: Text(
          'Now Playing: ' + widget.trackName,
          style: TextStyle(fontSize: 14.0),
        ),
      ),
      body: Container(
        decoration: BoxDecoration(
          // Box decoration takes a gradient
          gradient: LinearGradient(
            // Where the linear gradient begins and ends
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            // Add one stop for each color. Stops should increase from 0 to 1
            stops: [0.3, 0.6],
            colors: [
              // Colors are easy thanks to Flutter's Colors class.
              Colors.orange[200],
              Colors.pink[300],
            ],
          ),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(top: 40.0, bottom: 20.0),
                child: Center(
                    child: Image.network(
                        widget.artworkUrl100.replaceAll('100x100', '200x200')))),
            Container(
                margin: EdgeInsets.all(16.0),
                padding: EdgeInsets.all(16.0),
                width: MediaQuery.of(context).size.width,
                height: 120.0,
                decoration: BoxDecoration(
                    border: Border.all(width: 5.0, color: Colors.white),
                    borderRadius: BorderRadius.all(Radius.circular(20.0))),
                child: Wrap(
                  direction: Axis.vertical,
                  spacing: 4.0,
                  children: <Widget>[
                    widget.artistName != null ? Text(widget.artistName, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),) : Text('Empty Artist Name'),
                    widget.releaseDate != null ? Text(widget.releaseDate.substring(0, 4), style: TextStyle(color: Colors.white),) : Text('Empty Release date'),
                    widget.primaryGenreName != null ? Text(widget.primaryGenreName, style: TextStyle(color: Colors.white),) : Text('Empty Genre Name'),
                    widget.currency != null ? Text('${widget.currency} ${widget.trackPrice.toString()}',style: TextStyle(color: Colors.white),) : Text('Empty Currency'),
                  ],
                ))
          ], //
        ),
      ),
    );
  }
}
