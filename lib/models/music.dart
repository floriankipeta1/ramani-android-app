class Music {
  final String artistName;
  final String trackName;
  final String artworkUrl60;
  final String artworkUrl100;
  final double trackPrice;
  final String primaryGenreName;
  final String releaseDate;
  final String currency;

  Music({this.artistName, this.trackName, this.artworkUrl60, this.artworkUrl100, this.trackPrice, this.primaryGenreName, this.releaseDate, this.currency});

  factory Music.fromJson(Map<String, dynamic> json) {
    return Music(
      artistName: json['artistName'],
      trackName: json['trackName'],
      artworkUrl60: json['artworkUrl60'],
      artworkUrl100: json['artworkUrl100'],
      trackPrice: json['trackPrice'],
      primaryGenreName: json['primaryGenreName'],
      releaseDate: json['releaseDate'],
      currency: json['currency']
    );
  }
}